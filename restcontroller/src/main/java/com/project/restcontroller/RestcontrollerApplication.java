package com.project.restcontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.project.*")
public class RestcontrollerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestcontrollerApplication.class, args);
    }

}
